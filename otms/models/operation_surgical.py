from odoo import api, fields, models
from odoo.exceptions import ValidationError
from datetime import date, datetime, timedelta

import logging
_logger = logging.getLogger(__name__)


class OperationSurgical(models.Model):
    _name = 'otms.operation_surgical'
    _description = 'Operation Surgical'

    name = fields.Char(string='Surgery Name', required=True)

    patient_id = fields.Many2one(
        string='Patient',
        comodel_name='otms.patient',
        required=True,
        ondelete='restrict',
    )

    operation_theatre_id = fields.Many2one(
        string='Operation Theatre',
        comodel_name='otms.operation_theatre',
        ondelete='restrict',
        required=True,
    )
    
    planned_date = fields.Datetime(
        string='Planned date',
        required=True
    )

    actual_date = fields.Datetime(
        string='Actual date',
        readonly=True
    )
    
    end_date = fields.Datetime(
        string='End date',
        readonly=True
    )

    primary_doctor_id = fields.Many2one(
        string='Primary doctor',
        comodel_name='otms.employee',
        ondelete='restrict',
        required=True,
    )

    secondary_doctor_id = fields.Many2one(
        string='Secondary doctor',
        comodel_name='otms.employee',
        ondelete='restrict',
    )
    
    Anesthesiologist_id = fields.Many2one(
        string='Anesthesiologist',
        comodel_name='otms.employee',
        ondelete='restrict',
    )
  
    material_ids = fields.Many2many(
        string='Material',
        comodel_name='otms.material',
        relation='operation_surgical_material_rel',
    )

    state = fields.Selection([('pending', 'Pending'), ('in_progress', 'In Progress'),('finished','Finished')], string='State', default="pending", copy=False)

    report = fields.Text(string='Report',)
    
    def start_surgery(self):
        self.update({'state': 'in_progress'})
        self.update({'actual_date': fields.Datetime.now()})
        

    def end_surgery(self):
        self.update({'state': 'finished'})
        self.update({'end_date': fields.Datetime.now()})
    
    
