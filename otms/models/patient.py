
import re
from odoo import api, fields, models , _
from odoo.exceptions import ValidationError


import logging
_logger = logging.getLogger(__name__)

class Patient(models.Model):
    _name = 'otms.patient'
    _description = 'otms patient'

    # _rec_name = 'name'
    # _order = 'name ASC'

    name = fields.Char(string='Name', required=True, default=lambda self: _('New'), copy=False)
 
    phone_number = fields.Char(string='Phone number', required=True )

    birth_date = fields.Date(string='birth Date')

    gender = fields.Selection(string='gender',selection=[('male', 'Male'), ('female', 'Female')])
    
    # channel_ids = fields.Char('aasd')
    # channel_ids = fields.Many2many('mail.channel', 'table_name', 'id', 'channel_id', string='Channels', copy=False)
    # meeting_ids = fields.Many2many('calendar.event', 'table_names', 'res_partner_id',
    #                                'calendar_event_id', string='Meetings', copy=False)
    # meeting_ids = fields.Char('asd')
    # commercial_partner_id = fields.Many2one('res.partner', string='Commercial Entity', store=False, readonly=True,)
    # commercial_partner_id = fields.Char(string='asdasd')
    # commercial_partner_id = fields.Many2one(related='res.partner')

    @api.onchange('phone_number')
    def validate_phone(self):
        if self.phone_number:
            match = re.match('^[0-9]\d{10}$', self.phone_number)
            if not match:
                raise ValidationError('Invalid')
