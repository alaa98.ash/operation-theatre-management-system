from email.policy import default
from odoo import api, fields, models
from odoo.exceptions import ValidationError

import logging
_logger = logging.getLogger(__name__)

class Specialization(models.Model):
    _name = 'otms.specialization'
    _description = 'Specialization'

    
    name = fields.Char(
        string='name',
        required=True
    )
    
    
    # doctors_ids = fields.One2many(
    #     string='doctors',
    #     comodel_name='otms.employee',
    #     inverse=''  
    # )
    