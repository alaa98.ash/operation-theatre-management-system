from odoo import api, fields, models


class Employee(models.Model):
    _name = 'otms.employee'
    _inherit = 'hr.employee'
    
    _description = 'otms employee'
    # _inherits = {'hr.employee': 'name'}
    # company_id = fields.Char('cc')
        
    category_ids = fields.Char('ss')
    
    specialization_id = fields.Many2one(
        string='specialization',
        comodel_name='otms.specialization',
        ondelete='restrict',
    )
    
    company_id = fields.Many2one('res.company',required=False)
    # category_ids = fields.Many2many(
    #     'hra.employee.category', 'employee_category_rel',
    #     'emap_id', 'category_id', groups="hr.group_hr_manager",
    #     straing='Tags')