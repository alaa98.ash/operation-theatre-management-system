# -*- coding: utf-8 -*-

from . import models
from . import patient
from . import employee
from . import operation_theatre
from . import operation_surgical
from . import specialization
from . import instrument
# from . import *