from odoo import api, fields, models,_
from odoo.exceptions import ValidationError


import logging
_logger = logging.getLogger(__name__)

class Instrument(models.AbstractModel):
    _name = 'otms.instrument'

    name = fields.Char(string='name',required=True,default=lambda self: _('New'), copy=False)  
    quantity = fields.Integer(string='quantity')

    @api.onchange('quantity')
    def validate_quantity(self):
        if self.quantity<0:
            raise ValidationError('Quantity must be bigger than or equal to 0')
            

class Equipment(models.Model):
    _name = 'otms.equipment'
    _inherit = 'otms.instrument'   
    _description = 'equipment'


class Material(models.Model):
    _name = 'otms.material'
    _inherit = 'otms.instrument' 
    _description = 'Material'
    
    price = fields.Float(
        string='price',
    )
    
    @api.onchange('price')
    def validate_price(self):
        if self.price<0:
            raise ValidationError('price must be bigger than 0')
