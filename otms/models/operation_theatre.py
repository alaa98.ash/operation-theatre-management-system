from odoo import api, fields, models

import logging
_logger = logging.getLogger(__name__)


class OperationTheatre(models.Model):
    _name = 'otms.operation_theatre'
    _description = 'Operation Theatre'

    name = fields.Char(string='Name')
    state = fields.Char(string='Theatre State', readonly=True)
    
    equipment_ids = fields.Many2many(
                    string='equipments',
                    comodel_name='otms.equipment',
                    relation='operation_theater_equipment_rel',
                )
    
    # channel_ids = fields.Many2many('mail.channel', 'partner_id', 'channel_id', copy=False ) 
    # category_ids = fields.Char(string='hh')
    # active = fields.Boolean(string="Active", default=True)